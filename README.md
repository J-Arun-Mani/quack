# quack

Search the web without any fear via command-line and DuckDuckGo.
Photo by Ann H from Pexels.

## What Is It?

`quack` is a command-line application to search the web via [DuckDuckGo](https://duck.com).
It uses the [Instant Answer API](https://api.duckduckgo.com) to show you results.

## How Can I Get It?

Simple! Download it from the very repository! Go to [Tags](https://gitlab.com/J-Arun-Mani/quack/-/tags) and download the latest version yourself.
You might have to rename and put the binary in a comfortable location, preferably in `.local/bin` or anywhere that is in your `$PATH`.

## How Can I Use It?

Launch the Terminal, type executable name followed by the query. That's it.

```sh
$ quack "hello"
```

Don't like pretty formatted text? Use `--format=text`!

```sh
$ quack "hello" --format=text # Or simply -f text
```

## Show Me How It Looks

![Rich Format One](screenshots/rich_format_one.png "An example showing rich format ")
![Rich Format Two](screenshots/rich_format_two.png "Another example showing rich format ")
![Text Format](screenshots/text_format.png "Example showing text format ")
![JSON Format](screenshots/json_format.png "Example showing JSON format ")

## Output Format

`quack` by default uses rich format (the fancy one with bold, italic and underline). Be aware, your terminal should support color for this to work good.
If rich format is too much for your terminal (or you), you can use `--format=text`.
For some once in a blue moon reason, if you need JSON as output, you can use `--format=json`. This will be useful for piping the output to another program.

```sh
# When you lose hope in life
$ quack "hello" --format=json | python3 -m json.tool
```

## Background Information

`quack` is written in [Rust](https://www.rust-lang.org) - A language empowering everyone to build reliable and efficient software. (I was asked to include this line wherever I mention Rust).

The code is written in such a manner that you would expect from someone who directly jumped into coding after reading the documentation.

I have used modules for anything not too simple, like `serde_json` for JSON parsing (anybody here writes their own parser??) and `reqwest` for HTTP requests. And of course `clap` for CLI arguments parsing, printing help (`$ quake --help`) messages etc.

What else did I miss? Oh yea, `url` for encoding the parameters to pass to `reqwest` (Due to legal reasons, I can't disclose why I didn't use `reqwest`'s inbuilt method.) And finally `termcolor` for pretty printing output.

So pardon me for silly bugs, inefficient methods. The approach is pretty straightforward, but who knows…

## Building It Yourself

1. Clone the repository.
2. Execute `$ cargo build` for a binary with debug support.
3. Use `$ cargo build --release` for production ready binary.

## This Is Huge!

> OMG! Dude a simple CLI app takes 9 MB? Are you out of your mind? Our ancestors wrote programs that takes just a few bytes!…

I understand. It is pretty huge. When I checked out online, they said Rust focuses more on speed than binary size. Yes, I read documents that guide how to reduce it further, but I haven't played with it yet. So please bear with the size. As per a survey by myself, I have found that `quack` is the fastest CLI web search binary with lowest memory footprint. (Citation Needed).

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.

## Things Aside

Seriously speaking, I heart-fully welcome suggestions, bug fix and even usage. I owe you a lot if you do that. Also thanks to Rust, the dependencies, and clear and concise documentation for allowing me to write the app without any headache.

To flex, I wrote the code in my potato laptop with only 2 GB RAM. I used Doom Emacs (Emacs Rocks!) and Rust Analyzer as LSP. I can spend the entire day admiring how beautiful they integrate and work. Emacs Rocks!

## And One More Thing…

Strictly speaking, due to limitations of DuckDuckGo's API, it is not possible to show _pixel-perfect_ results like the search engine. But the app can save you from opening browser for every small _fast lookup_.

## Finally

Unless otherwise explicitly mentioned, my tone of speech should be taken as humor.

**Enjoy!** and **Thanks For Reading!**
