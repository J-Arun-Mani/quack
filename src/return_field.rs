use serde::{Deserialize, Serialize};
use serde_json::Value;

#[allow(dead_code)]
#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct IconField {
    pub URL: String,
    #[serde(skip)]
    pub Height: i32,
    #[serde(skip)]
    pub Width: i32,
}

#[allow(dead_code)]
#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct ResultField {
    pub FirstURL: Option<String>,
    pub Icon: Option<IconField>,
    pub Result: Option<String>,
    pub Text: Option<String>,
}

#[allow(dead_code)]
#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct ReturnField {
    pub Abstract: String,
    pub AbstractSource: String,
    pub AbstractText: String,
    pub AbstractURL: String,
    pub Answer: Value,
    pub AnswerType: String,
    pub Definition: String,
    pub DefinitionSource: String,
    pub DefinitionURL: String,
    pub Entity: String,
    pub Heading: String,
    pub Image: String,
    #[serde(skip)]
    pub ImageHeight: i32,
    #[serde(skip)]
    pub ImageIsLogo: i32,
    #[serde(skip)]
    pub ImageWidth: i32,
    pub Infobox: Value,
    pub Redirect: String,
    pub RelatedTopics: Vec<ResultField>,
    pub Results: Vec<ResultField>,
    pub Type: String,
    #[serde(skip)]
    pub meta: Value,
}
