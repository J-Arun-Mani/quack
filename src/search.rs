// quack - Search the web without any fear via command-line and DuckDuckGo.
// Copyright (C) 2021 J Arun Mani <j.arunman@protonmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use crate::return_field::ReturnField;
use reqwest;
use serde_json;
use url::Url;

pub fn search(query: String) -> Result<(ReturnField, String), Box<dyn std::error::Error>> {
    let url = Url::parse_with_params(
        "https://api.duckduckgo.com/?format=json&skip_disambig=1&no_redirect=1&t=quack",
        &[("q", query.clone())],
    )?;
    let encd_url = url.as_str();
    let text = reqwest::blocking::get(encd_url)?.text()?;
    let retfld: ReturnField = serde_json::from_str(&text)?;
    let res_url = String::from(
        Url::parse_with_params("https://duckduckgo.com/?t=quack", &[("q", query)])?.as_str(),
    );
    Ok((retfld, res_url))
}
