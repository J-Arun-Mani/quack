// quack - Search the web without any fear via command-line and DuckDuckGo.
// Copyright (C) 2021 J Arun Mani <j.arunman@protonmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use crate::return_field::ReturnField;
use serde_json;
use std::io::Write;
use termcolor::{ColorChoice, ColorSpec, StandardStream, WriteColor};

pub fn display_json(retfld: ReturnField, _url: String) -> Result<(), Box<dyn std::error::Error>> {
    let json = serde_json::to_string_pretty(&retfld)?;
    println!("{}", json);
    Ok(())
}

pub fn display_rich_text(
    retfld: ReturnField,
    url: String,
) -> Result<(), Box<dyn std::error::Error>> {
    let choice = ColorChoice::Always;
    write_text(retfld, url, choice)
}

pub fn display_text(retfld: ReturnField, url: String) -> Result<(), Box<dyn std::error::Error>> {
    let choice = ColorChoice::Never;
    write_text(retfld, url, choice)
}

fn write_text(
    retfld: ReturnField,
    url: String,
    choice: ColorChoice,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut found = 0;
    let mut spec = ColorSpec::new();
    let mut stdout = StandardStream::stdout(choice);

    if !retfld.Heading.is_empty() {
        write!(&mut stdout, "‣ ")?;
        stdout.set_color(spec.set_bold(true))?;
        write!(&mut stdout, "{}\n", retfld.Heading)?;
        stdout.set_color(spec.set_bold(false))?;
        write!(&mut stdout, "   {}\n\n", retfld.AbstractText)?;
        found += 1;
    }

    if !retfld.AbstractSource.is_empty() {
        write!(&mut stdout, "⁃ ")?;
        stdout.set_color(spec.set_italic(true))?;
        write!(&mut stdout, "{}\n", retfld.AbstractSource)?;
        stdout.set_color(spec.set_italic(false))?;
        write!(&mut stdout, "   (")?;
        stdout.set_color(spec.set_underline(true))?;
        write!(&mut stdout, "{}", retfld.AbstractURL)?;
        stdout.set_color(spec.set_underline(false))?;
        write!(&mut stdout, ")\n\n")?;
        found += 1;
    }

    if !retfld.Definition.is_empty() {
        write!(&mut stdout, "⇥ {}\n", retfld.Definition)?;
        stdout.set_color(spec.set_italic(true))?;
        write!(&mut stdout, "   {}\n", retfld.DefinitionSource)?;
        stdout.set_color(spec.set_italic(false))?;
        write!(&mut stdout, "   (")?;
        stdout.set_color(spec.set_underline(true))?;
        write!(&mut stdout, "{}", retfld.DefinitionURL)?;
        stdout.set_color(spec.set_underline(false))?;
        write!(&mut stdout, ")\n\n")?;
        found += 1;
    }

    if let serde_json::Value::Object(infobox) = retfld.Infobox {
        let contents: Vec<&serde_json::Value> = infobox["content"]
            .as_array()
            .unwrap()
            .iter()
            .filter(|content| content["data_type"] == "string")
            .collect();
        if contents.len() > 0 {
            write!(&mut stdout, "✦ ")?;
            stdout.set_color(spec.set_bold(true))?;
            write!(
                &mut stdout,
                "{}\n",
                infobox["meta"][0]["value"].as_str().unwrap_or_default()
            )?;
            stdout.set_color(spec.set_bold(false))?;
            for content in contents {
                write!(
                    &mut stdout,
                    "   {} - {}\n",
                    content["label"].as_str().unwrap(),
                    content["value"].as_str().unwrap()
                )?;
            }
            write!(&mut stdout, "\n")?;
        }
    }

    for resfld in retfld.RelatedTopics {
        let text = resfld.Text.unwrap_or_default();
        let url = resfld.FirstURL.unwrap_or_default();
        if !text.is_empty() {
            write!(&mut stdout, "• {}\n", text)?;
            write!(&mut stdout, "   (")?;
            stdout.set_color(spec.set_underline(true))?;
            write!(&mut stdout, "{}", url)?;
            stdout.set_color(spec.set_underline(false))?;
            write!(&mut stdout, ")\n\n")?;
            found += 1;
        }
    }

    if found > 0 {
        write!(&mut stdout, "Proudly powered by ",)?;
        stdout.set_color(spec.set_bold(true))?;
        write!(&mut stdout, "DuckDuckGo 🦆\n")?;
        stdout.set_color(spec.set_bold(false))?;
        write!(&mut stdout, "See more: ")?;
    } else {
        write!(
            &mut stdout,
            "I'm afraid I don't have anything to tell you… \nBut hey! Check the original ",
        )?;
        stdout.set_color(spec.set_bold(true))?;
        write!(&mut stdout, "DuckDuckGo 🦆")?;
        stdout.set_color(spec.set_bold(false))?;
        write!(&mut stdout, ": ")?;
    }

    stdout.set_color(spec.set_underline(true))?;
    write!(&mut stdout, "{}\n", url)?;
    stdout.set_color(spec.set_underline(false))?;

    Ok(())
}
