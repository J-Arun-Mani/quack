// quack - Search the web without any fear via command-line and DuckDuckGo.
// Copyright (C) 2021 J Arun Mani <j.arunman@protonmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use clap::Clap;
use quack::{
    display::{display_json, display_rich_text, display_text},
    options::Format,
    options::Options,
    search::search,
};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let options = Options::parse();
    let (retfld, url) = search(options.query)?;

    match options.format {
        Format::JSON => display_json(retfld, url)?,
        Format::RichText => display_rich_text(retfld, url)?,
        Format::Text => display_text(retfld, url)?,
    }

    Ok(())
}
